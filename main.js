/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50 */
/*global define, $, brackets, window, document, location*/

define(function (require, exports, module) {
    'use strict';

    // Brckets modules
    var CommandManager = brackets.getModule('command/CommandManager'),
        Menus = brackets.getModule('command/Menus'),
        AppInit = brackets.getModule('utils/AppInit'),
        ExtensionUtils = brackets.getModule('utils/ExtensionUtils'),
        PanelManager = brackets.getModule('view/PanelManager'),
        Mustache = brackets.getModule('thirdparty/mustache/mustache'),
        terminalTemplate = require('text!templates/terminalTemplate.html'),
        fit = require('addons/fit/fit'),
        attach = require('addons/attach/attach'),
        fullscreen = require('addons/fullscreen/fullscreen'),
        Terminal = require('src/xterm'),
        panel,
        number = 1;

    ExtensionUtils.loadStyleSheet(module, 'styles/style.css');
    ExtensionUtils.loadStyleSheet(module, 'src/xterm.css');

    var term,
        protocol,
        socketURL,
        socket,
        pid,
        charWidth = 10,
        charHeight = 18,
        rows = 2,
        cols = 190;

    var $addButton,
        $closeButton,
        $tabHeader,
        $tabContent;

    function createTerminal(terminalContainer, terminalNumber) {

        // Clean terminal
        while (terminalContainer.children.length) {
            terminalContainer.removeChild(terminalContainer.children[0]);
        }

        term = new Terminal({
            cursorBlink: true
        });
        term.on('resize', function (size) {
            if (!pid) {
                return;
            }
            var cols = size.cols,
                rows = size.rows,
                url = '/terminals/' + pid + '/size?cols=' + cols + '&rows=' + rows;

            $.ajax({
                type: 'POST',
                url: url,
            });
        });

        protocol = (location.protocol === 'https:') ? 'wss://' : 'ws://';
        socketURL = protocol + location.hostname + ((location.port) ? (':' + location.port) : '') + '/terminals/';
        socketURL = socketURL.replace('preview', 'wss');

        term.open(terminalContainer);

        /*term.fit();*/

        var url = 'https://wss-aivaras-dev.nodespeed.io/terminals?cols=' + cols + '&rows=' + rows;
        var xhr = createCORSRequest('GET', url);

        xhr.onload = function () {
            var pid = xhr.responseText;
            window.pid = pid;
            socketURL += pid;
            socket = new WebSocket(socketURL);
            socket.onopen = function () {
                runTerminal(terminalNumber);
            };
            socket.onclose = function (mes) {
                console.log(mes);
            };
            socket.onerror = function (err) {
                console.log(err);
            };
        };

        xhr.onerror = function (err) {
            console.log(err);
        };

        xhr.send();
    }

    function runTerminal(terminalNumber) {
        term.attach(socket);
        term._initialized = true;

        setActive(terminalNumber);
        number++;
        panel.show();
    }

    var toggleTerminal = function () {
        if (panel.isVisible()) {
            panel.hide();
        } else {
            if ($tabHeader.children().length > 1) {
                panel.show();
            } else {
                addTerminal();
            }
        }
    };

    var setActive = function (tabNumber) {
        if (tabNumber) {
            $tabHeader.children().removeClass('active');
            $tabContent.children().removeClass('active');

            $('#tab' + tabNumber).addClass('active');
            $('#tab-header-' + tabNumber).addClass('active');
        } else {
            $tabHeader.children().last().addClass('active');
            $tabContent.children().last().addClass('active');
        }
    }

    function createCORSRequest(method, url) {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
            xhr.open(method, url, true);
        } else if (typeof XDomainRequest != "undefined") {
            xhr = new XDomainRequest();
            xhr.open(method, url);
        } else {
            xhr = null;
        }
        return xhr;
    }

    var addTerminal = function () {

        $tabHeader.append(
            $('<li>').attr({
                'id': 'tab-header-' + number
            })
            .append(
                $('<a>').attr({
                    'class': 'tab-link',
                    'href': '#tab' + number,
                    'data-toggle': 'tab'
                })
                .append('Terminal-' + number)
                .append($('<span>').attr({
                    'id': 'close-button-' + number,
                    'class': 'circle-button',
                }).append('x'))
            ));

        $tabContent.append(
            $('<div>').attr({
                'class': 'tab-pane full-height',
                'id': 'tab' + number
            }).append(
                $('<div>').attr({
                    'id': 'terminal-container-' + number,
                    'class': 'terminal-container'
                })
            ));

        var terminalContainer = $('#terminal-container-' + number)[0];

        createTerminal(terminalContainer, number);
    }

    var removeTerminal = function (terminalNumber) {
        var $header = $('#tab-header-' + terminalNumber)[0];
        var $body = $('#tab' + terminalNumber)[0];

        $header.parentNode.removeChild($header);
        $body.parentNode.removeChild($body);

        if ($tabHeader.children().length > 1 && $tabHeader.find('li.active').length === 0) {
            setActive();
        }
    }

    var hideTerminal = function () {
        panel.hide();
    }

    AppInit.htmlReady(function () {

        var context_menus = Menus.getMenu(Menus.AppMenuBar.FILE_MENU);
        var OPEN_TERMINAL = 'xtermTerminal.open';

        CommandManager.register('Toggle terminal', OPEN_TERMINAL, toggleTerminal);
        context_menus.addMenuDivider();
        context_menus.addMenuItem(OPEN_TERMINAL, null, Menus.LAST);

        panel = PanelManager.createBottomPanel(OPEN_TERMINAL, $(terminalTemplate), 200);

        $addButton = $('#add-button');
        $closeButton = $('#close-button');
        $tabHeader = $('#tab-header');
        $tabContent = $('#tab-content');

        $addButton.click(addTerminal);
        $closeButton.click(hideTerminal);
        $(document).on('click', '#tab-header li span', function (e) {
            var parts = e.currentTarget.id.split('-');
            removeTerminal(parts[2]);
        });
    });
});